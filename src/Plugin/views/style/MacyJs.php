<?php

namespace Drupal\macyjs\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Displays content using Macy.js.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "macyjs",
 *   title = @Translation("Macy.js"),
 *   help = @Translation("Macy.js is a lightweight dependency-free JavaScript library designed to sort items vertically into columns by finding an optimum layout with a minimum height."),
 *   theme = "views_view_macyjs",
 *   display_types = {"normal"}
 * )
 */
class MacyJs extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Does the style plugin support grouping of rows.
   *
   * @var bool
   */
  protected $usesGrouping = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['trueOrder'] = ['default' => 0];
    $options['waitForImages'] = ['default' => 0];
    $options['useOwnImageLoader'] = ['default' => 0];
    $options['mobileFirst'] = ['default' => 1];
    $options['margin_x'] = ['default' => 0];
    $options['margin_y'] = ['default' => 0];
    $options['macyjs_columns'] = ['default' => '1'];
    $options['breakAt'] = ['default' => "1440: 4,\n1200: 4,\n1024: 4,\n640: 3,\n360: 2,"];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['trueOrder'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('True order'),
      '#default_value' => $this->options['trueOrder'],
    ];

    $form['waitForImages'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Wait for images'),
      '#default_value' => $this->options['waitForImages'],
    ];

    $form['useOwnImageLoader'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use own image loader'),
      '#default_value' => $this->options['useOwnImageLoader'],
    ];

    $form['mobileFirst'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Mobile first'),
      '#default_value' => $this->options['mobileFirst'],
    ];

    $form['margin_x'] = [
      '#type' => 'number',
      '#title' => $this->t('X Margin'),
      '#default_value' => $this->options['margin_x'],
    ];

    $form['margin_y'] = [
      '#type' => 'number',
      '#title' => $this->t('Y Margin'),
      '#default_value' => $this->options['margin_y'],
    ];

    $form['macyjs_columns'] = [
      '#type' => 'number',
      '#title' => $this->t('Columns'),
      '#default_value' => $this->options['macyjs_columns'],
    ];

    $form['breakAt'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Break at'),
      '#default_value' => $this->options['breakAt'],
      '#description' => $this->t('Syntax:<br>640: 3,<br>360: 2,<br>or<br>640:{margin:{x:20,y:10,},columns:3},<br>360:{margin:{x:0,y:0,},columns:1},'),
    ];

  }

}
