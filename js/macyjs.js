(function ($, Drupal, drupalSettings) {

  'use strict';

  var initializedMacy = [];

  Drupal.behaviors.macyjs = {
    attach: function (context, settings) {
        if (!isset(settings.macyjs)) {
          console.error("Missing macyjs.");
        }

        if (initializedMacy.length === Object.keys(settings.macyjs).length) {
          return;
        }

        for (var domId in settings.macyjs) {
          if (initializedMacy.indexOf(domId) > -1) {
            continue;
          }

          var macyInitResult = initializeMacyJs(settings.macyjs[domId]);

          if (isset(macyInitResult)) {
            initializedMacy.push(domId);
          }
        }

    }
  }

  function initializeMacyJs(settings) {
    try {
      return Macy(settings);
    }
    catch (e) {
      console.log(e);
      return null;
    }

  }

  function isset(value) {
    return !(value === null || typeof value === "undefined");
  }

})(jQuery, Drupal, drupalSettings);
